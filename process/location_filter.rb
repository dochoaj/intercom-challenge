# frozen_string_literal: true

require_relative '../services/txt_reader'
require_relative '../services/user_parser'
require_relative '../services/data_output'
require_relative '../services/distance_calculator'

module Process
  # A process who filter users based on its location in reference
  # to a center position.
  class LocationFilter
    def initialize(params)
      validate_params(params)
      @source_url = params[:source_url]
      @file_path = params[:file_path]
      @distance_limit = params[:distance_limit] || 100
      @center_lat = params[:center_lat] ? params[:center_lat].to_f : 53.339428
      @center_lon = params[:center_lon] ? params[:center_lon].to_f : -6.257664
      @center_lat_lon = [@center_lat, @center_lon]
      @keys = %w[user_id name]
      fetch_users
    end

    def params
      {
        source_url: @source_url,
        file_path: @file_path,
        distance_limit: @distance_limit,
        center_lat: @center_lat,
        center_lon: @center_lon,
        center_lat_lon: @center_lat_lon,
        keys: @keys
      }
    end

    def validate_params(params)
      valid_path = params[:source_url] || params[:file_path]
      unless valid_path
        raise 'You need to define a source: source_url or file_path is required'
      end
      distance_limit_err = 'Distance limit should be greater or equals than 0'
      raise distance_limit_err if params[:distance_limit]&.negative?
    end

    def run
      present_users select_by_limit(@distance_limit)
    end

    def select_by_limit(limit)
      @users.select do |u|
        distance_calculator.for(
          @center_lat_lon, [u['latitude'], u['longitude']]
        ) <= limit
      end
    end

    def present_users(users)
      outputter.export(users, @keys)
    end

    def fetch_users
      if @file_path
        rows = reader.from_file(@file_path)
      elsif @source_url
        rows = reader.from_url(@source_url)
      end
      @users = parser.parse_lines(
        rows
      ).sort_by { |e| e['user_id'] }
      @users
    end

    def reader
      Services::TxtReader
    end

    def parser
      Services::UserParser
    end

    def outputter
      Services::DataOutput
    end

    def distance_calculator
      Services::DistanceCalculator
    end
  end
end
