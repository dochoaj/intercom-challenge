require 'haversine'
# frozen_string_literal: true

module Services
  class DistanceCalculationError < StandardError
  end
end

module Services
  # Parses one string line into a parametrized hash
  class DistanceCalculator
    def self.for(source, destination)
      validate(source)
      validate(destination)
      Haversine.distance(source, destination).to_kilometers.round(2)
    end

    def self.validate(input)
      error('Distance should be an array') unless input.is_a? Array
      error('Array should contains two elements') unless input.length == 2
      all_float = input.all? { |e| e.is_a? Float }
      error('All elements should be floats') unless all_float
      true
    end

    def self.error(message = '')
      raise error_class, message
    end

    def self.error_class
      Services::DistanceCalculationError
    end
  end
end
