require 'json'
# frozen_string_literal: true

module Services
  class ParsingError < StandardError
  end
end

module Services
  # Parses one string line into a parametrized hash
  class UserParser
    def self.parse_lines(lines)
      valid = lines.is_a?(Array) && lines.all? { |l| l.is_a?(String) }
      error("Parameter must be a String array, received #{lines}") unless valid
      lines.map { |el| parse_line(el) }
    end

    def self.parse_line(line)
      parsed_hash = JSON.parse(line)
      unless parse_valid? parsed_hash
        error('Hash is incomplete. Some expected keys are missing')
      end
      correct_formats(parsed_hash)
    rescue JSON::ParserError
      error('JSON Parsing error, verify source syntax')
    rescue TypeError
      error('JSON Parsing error, verify source syntax')
    end

    def self.correct_formats(hash)
      hash['latitude'] = hash['latitude'].to_f
      hash['longitude'] = hash['longitude'].to_f
      hash
    end

    def self.error(message = '')
      raise error_class, message
    end

    def self.error_class
      Services::ParsingError
    end

    def self.parse_valid?(parsed)
      return false if !parsed || parsed.keys.empty?

      expected_keys.each do |key|
        return false unless parsed.key? key
      end

      true
    end

    def self.expected_keys
      %w[name latitude longitude user_id]
    end
  end
end
