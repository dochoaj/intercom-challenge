# frozen_string_literal: true

module Services
  class DataOutputError < StandardError
  end
end

module Services
  # Outputs an array of hashes into a defined format
  class DataOutput
    def self.export(data, exported_keys = [], format = 'stdout')
      validate_data(data)
      validate_format(format)
      validate_exported_keys(exported_keys)
      send(method_name_for(format), data, exported_keys)
    end

    def self.validate_data(data)
      error('Data should be an Array') unless data.is_a? Array
      are_all_hash = data.all? { |d| d.is_a? Hash }
      error('All data elements must be a Hash') unless are_all_hash
    end

    def self.validate_format(format)
      error('Format should be a string') unless format.is_a? String
      error("Format #{format} is not supported") unless format_available? format
    end

    def self.validate_exported_keys(exported_keys)
      error('Exported keys should be an Array') unless exported_keys.is_a? Array
      are_all_string = exported_keys.all? { |d| d.is_a? String }
      error('All exported keys elements must be Strings') unless are_all_string
    end

    def self.format_available?(format)
      respond_to? method_name_for format
    end

    def self.method_name_for(format)
      unless format.is_a? String
        error("Format: Expected string, #{format.class} received")
      end

      "export_#{format}".to_sym
    end

    def self.extract_keys_from_data(data, exported_keys)
      return data if exported_keys.length.zero?
      data.map do |d|
        d.select { |k, _| exported_keys.include? k.to_s }
      end
    end

    def self.export_stdout(data, exported_keys)
      str = extract_keys_from_data(data, exported_keys).map do |e|
        res = if exported_keys.length.zero?
                e.keys.map { |key| e[key] }
              else
                exported_keys.map { |key| e[key] }
              end
        res.join(' - ')
      end.join("\n")

      puts str
      str
    end

    def self.error(message = '')
      raise error_class, message
    end

    def self.error_class
      Services::DataOutputError
    end
  end
end
