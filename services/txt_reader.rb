require 'rest-client'
# frozen_string_literal: true

module Services
  class ReadError < StandardError
  end
end

module Services
  # Read a txt file from a location and outputs an array with the lines
  class TxtReader
    # If we have a HUGE file from internet may be impossible to load it all on
    # memory. So this method streams the content to a intermediate temp
    # file on disk, slower but safer. However, large amounts of data should
    # not be handled this way, we should need a queue or a Map/Reduce approach.
    def self.from_url(url = '')
      error('Invalid url') unless url
      raw = RestClient::Request.execute(
        method: :get, url: url, raw_response: true
      )
      from_file(raw.file.path)
    rescue SocketError
      error("Unable to read online file at #{url}")
    end

    def self.from_file(path = '')
      error('Invalid path') unless path
      File.open(path).map(&:to_s)
    rescue Errno::ENOENT
      error("Unable to read local file at #{path}")
    end

    def self.error(message)
      raise error_class, message
    end

    def self.error_class
      Services::ReadError
    end
  end
end
