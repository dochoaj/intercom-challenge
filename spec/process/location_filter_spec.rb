# frozen_string_literal: true

require_relative '../../process/location_filter'
require_relative '../../services/all'

RSpec.describe Process::LocationFilter do
  describe 'instance methods' do
    before(:each) do
      allow(Services::TxtReader).to(
        receive(:from_file).and_return(
          [
            '{"latitude": "51.92893", "user_id": 10, "name": "Daniel 1", "longitude": "-6.043701"}',
            '{"latitude": "51.92893", "user_id": 2, "name": "Daniel 2", "longitude": "-6.043701"}'
          ]
        )
      )

      allow(Services::TxtReader).to(
        receive(:from_url).and_return(
          [
            '{"latitude": "51.92893", "user_id": 2, "name": "Daniel 2", "longitude": "-6.043701"}',
          ]
        )
      )

      @instance = Process::LocationFilter.new(
        file_path: 'my_path'
      )
    end

    describe 'params' do
      it 'must be defined' do
        expect(@instance.respond_to?(:params)).to be true
      end

      it 'must return a hash' do
        expect(@instance.params.class).to eq(Hash)
      end

      it 'must return consistent key' do
        expect(@instance.params[:keys]).to eq(%w[user_id name])
      end

      describe 'when constructing with default' do
        it 'must have default distance equals to 100' do
          expect(@instance.params[:distance_limit]).to eq(100)
        end

        it 'must have default center_lat equals to 53.339428' do
          expect(@instance.params[:center_lat]).to eq(53.339428)
        end

        it 'must have default center_lon equals to -6.257664' do
          expect(@instance.params[:center_lon]).to eq(-6.257664)
        end

        it 'must have consistent center_lat_long prop' do
          expect(@instance.params[:center_lat_lon]).to eq(
            [53.339428, -6.257664]
          )
        end
      end
    end

    describe 'validate_params' do
      it 'must be defined' do
        expect(@instance.respond_to?(:validate_params)).to be true
      end

      it 'should raise when source_url and file_path are undefined' do
        expect do
          @instance.validate_params({})
        end.to raise_error RuntimeError

        expect do
          @instance.validate_params(source_url: '')
        end.not_to raise_error

        expect do
          @instance.validate_params(file_path: '')
        end.not_to raise_error
      end

      it 'should raise when distance_limit is lower than zero' do
        expect do
          @instance.validate_params(source_url: '', distance_limit: -10)
        end.to raise_error RuntimeError

        expect do
          @instance.validate_params(source_url: '', distance_limit: 0)
        end.not_to raise_error

        expect do
          @instance.validate_params(source_url: '', distance_limit: 10)
        end.not_to raise_error
      end
    end

    describe 'run' do
      it 'must be defined' do
        expect(@instance.respond_to?(:run)).to be true
      end

      it 'should call present_users with correct arguments' do
        expect(@instance).to receive(:present_users).with(
          @instance.select_by_limit(10)
        )

        @instance.run
      end
    end

    describe 'select_by_limit' do
      it 'must be defined' do
        expect(@instance.respond_to?(:select_by_limit)).to be true
      end

      it 'should return zero results when distance limit is zero ' do
        expect(@instance.select_by_limit(0).length).to eq 0
      end

      it 'should return all results when distance limit is insanely high ' do
        expect(
          @instance.select_by_limit(100_000_000_000_000_000).length
        ).to eq 2
      end
    end

    describe 'present_users' do
      before(:each) do
        @users = [
          { 'user_id': 10, name: 'Daniel 1' },
          { 'user_id': 2, name: 'Daniel 2' },
          { 'user_id': 3, name: 'Daniel 3' },
        ]
      end

      it 'must be defined' do
        expect(@instance.respond_to?(:present_users)).to be true
      end

      it 'all calls should be consistent' do
        expect(@instance.outputter).to receive(:export).with(
          @users, @instance.params[:keys]
        )
        @instance.present_users(@users)
      end
    end

    describe 'fetch_users' do
      it 'must be defined' do
        expect(@instance.respond_to?(:fetch_users)).to be true
      end

      it 'must return users sorted by user_id key' do
        allow(Services::UserParser).to(
          receive(:parse_lines).and_return(
            [
              { 'user_id' => 10 },
              { 'user_id' => 2 },
              { 'user_id' => 3 }
            ]
          )
        )
        response = @instance.fetch_users
        expect(response.length).to eq(3)
        expect(response[0]['user_id'] <= response[1]['user_id']).to be true
      end

      describe 'when having initialization with source_url' do
        it 'called method to fetch data should be consistent' do
          allow(Services::TxtReader).to(
            receive(:from_file).and_return(
              [
                '{"latitude": "51.92893", "user_id": 1, "name": "Daniel 1", "longitude": "-6.043701"}',
                '{"latitude": "51.92893", "user_id": 2, "name": "Daniel 2", "longitude": "-6.043701"}'
              ]
            )
          )

          file = 'my_path'
          expect(
            Services::TxtReader
          ).to receive(:from_file).with(file)
          Process::LocationFilter.new(file_path: file)
        end
      end

      describe 'when having initialization with source_url' do
        it 'called method to fetch data should be consistent' do
          allow(Services::TxtReader).to(
            receive(:from_url).and_return(
              [
                '{"latitude": "51.92893", "user_id": 1, "name": "Daniel 1", "longitude": "-6.043701"}',
                '{"latitude": "51.92893", "user_id": 2, "name": "Daniel 2", "longitude": "-6.043701"}'
              ]
            )
          )

          url = 'my_path'
          expect(
            Services::TxtReader
          ).to receive(:from_url).with(url)
          Process::LocationFilter.new(source_url: url)
        end
      end

      it 'must call parser.parse_lines method' do
        allow(Services::UserParser).to(
          receive(:parse_lines).and_return(
            [
              { 'user_id': 10, name: 'Daniel 1' },
              { 'user_id': 2, name: 'Daniel 2' },
              { 'user_id': 3, name: 'Daniel 3' },
            ]
          )
        )

        expect(
          Services::UserParser
        ).to receive(:parse_lines)
        @instance.fetch_users
      end
    end

    describe 'reader' do
      it 'must be defined' do
        expect(@instance.respond_to?(:reader)).to be true
      end

      it 'must return an instance of Services::TxtReader' do
        expect(@instance.reader).to eq(Services::TxtReader)
      end
    end

    describe 'parser' do
      it 'must be defined' do
        expect(@instance.respond_to?(:parser)).to be true
      end

      it 'must return an instance of Services::UserParser' do
        expect(@instance.parser).to eq(Services::UserParser)
      end
    end

    describe 'outputter' do
      it 'must be defined' do
        expect(@instance.respond_to?(:outputter)).to be true
      end

      it 'must return an instance of Services::DataOutput' do
        expect(@instance.outputter).to eq(Services::DataOutput)
      end
    end

    describe 'distance_calculator' do
      it 'must be defined' do
        expect(@instance.respond_to?(:distance_calculator)).to be true
      end

      it 'must return an instance of Services::DistanceCalculator' do
        expect(@instance.distance_calculator).to eq(Services::DistanceCalculator)
      end
    end
  end
end
