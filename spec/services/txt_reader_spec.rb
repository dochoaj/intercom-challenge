# frozen_string_literal: true

require_relative '../../services/txt_reader'

RSpec.describe Services::TxtReader do
  describe 'from_url' do
    it 'should be defined' do
      expect(Services::TxtReader.respond_to?(:from_url)).to be true
    end

    it 'should respond an array of strings' do
      file = double('file')
      raw = double('raw')
      allow(raw).to receive_messages(file: file)
      allow(file).to receive_messages(path: 'path')
      allow(RestClient::Request).to receive(:execute).and_return(raw)
      allow(File).to receive(:open).with('path').and_return(%w[la le li])
      response = Services::TxtReader.from_url('file_url')
      expect(response.class).to eq(Array)
      expect(response.all? { |el| el.is_a? String }).to be true
    end

    it 'should raise an error when accessing an url who does not exist' do
      expect do
        Services::TxtReader.from_url('file_url')
      end.to raise_error Services::ReadError

      expect do
        Services::TxtReader.from_url(nil)
      end.to raise_error Services::ReadError
    end
  end

  describe 'from_file' do
    it 'should be defined' do
      expect(Services::TxtReader.respond_to?(:from_file)).to be true
    end

    it 'should respond an array of strings' do
      allow(File).to receive(:open).with('file_url').and_return(%w[la le li])
      response = Services::TxtReader.from_file('file_url')
      expect(response.class).to eq(Array)
      expect(response.all? { |el| el.is_a? String }).to be true
    end

    it 'should raise an error when accessing a file who does not exist' do
      expect do
        Services::TxtReader.from_file('file_url')
      end.to raise_error Services::ReadError

      expect do
        Services::TxtReader.from_file(nil)
      end.to raise_error Services::ReadError
    end
  end

  describe 'error' do
    it 'should be defined' do
      expect(Services::TxtReader.respond_to?(:error)).to be true
    end

    it 'should raise error_class with message' do
      message = 'Hello error'
      expect do
        Services::TxtReader.error(message)
      end.to raise_error Services::TxtReader.error_class, message
    end

    it 'should raise error_class with nil message' do
      message = nil
      expect do
        Services::TxtReader.error(message)
      end.to raise_error Services::TxtReader.error_class
    end
  end

  describe 'error_class' do
    it 'should be defined' do
      expect(Services::TxtReader.respond_to?(:error_class)).to be true
    end

    it 'should respond with Services::ParsingError' do
      expect(Services::TxtReader.error_class).to eq(Services::ReadError)
    end
  end
end
