# frozen_string_literal: true

require_relative '../../services/distance_calculator'

RSpec.describe Services::DistanceCalculator do
  describe 'for' do
    it 'should be defined' do
      expect(Services::DistanceCalculator.respond_to?(:for)).to be true
    end

    describe 'when parameters are correct' do
      before(:each) do
        @a = [52.986375, -6.043701]
        @b = [51.2, -5.0]
        @response = Services::DistanceCalculator.for(@a, @b)
      end

      it 'should return a float representing distance' do
        expect(@response.is_a?(Float)).to be true
      end
    end

    describe 'when both distance are equal' do
      it 'distance should be zero' do
        expect(
          Services::DistanceCalculator.for([51.2, 5.2], [51.2, 5.2])
        ).to eq 0.0
      end
    end

    describe 'when distance are different' do
      it 'distance should be distinct to zero' do
        expect(
          Services::DistanceCalculator.for([51.1, 5.0], [51.2, 5.2])
        ).to_not eq 0.0
      end
    end
  end

  describe 'validate' do
    it 'should be defined' do
      expect(Services::DistanceCalculator.respond_to?(:validate)).to be true
    end

    describe 'when validating' do
      describe 'and parameter is nil' do
        it 'should raise a Distance should be an array exception' do
          expect do
            Services::DistanceCalculator.validate(nil)
          end.to raise_error(
            Services::DistanceCalculator.error_class,
            'Distance should be an array'
          )
        end
      end

      describe 'and parameter is an array without two elements' do
        it 'should raise a Array should contains two elements' do
          expect do
            Services::DistanceCalculator.validate([1, 2, 3])
          end.to raise_error(
            Services::DistanceCalculator.error_class,
            'Array should contains two elements'
          )
        end
      end

      describe 'and parameter is an array without only floats' do
        it 'should raise a Array should contains two elements' do
          expect do
            Services::DistanceCalculator.validate([1, 2])
          end.to raise_error(
            Services::DistanceCalculator.error_class,
            'All elements should be floats'
          )
        end
      end

      describe 'and parameter is an array of two floats' do
        it 'should return true' do
          expect(Services::DistanceCalculator.validate([1.0, 2.0])).to be true
        end
      end
    end
  end

  describe 'error' do
    it 'should be defined' do
      expect(Services::DistanceCalculator.respond_to?(:error)).to be true
    end

    it 'should raise error_class with message' do
      message = 'Hello error'
      expect do
        Services::DistanceCalculator.error(message)
      end.to raise_error Services::DistanceCalculator.error_class, message
    end

    it 'should raise error_class with nil message' do
      message = nil
      expect do
        Services::DistanceCalculator.error(message)
      end.to raise_error Services::DistanceCalculator.error_class
    end
  end

  describe 'error_class' do
    it 'should be defined' do
      expect(Services::DistanceCalculator.respond_to?(:error_class)).to be true
    end

    it 'should respond with Services::ParsingError' do
      expect(
        Services::DistanceCalculator.error_class
      ).to eq(Services::DistanceCalculationError)
    end
  end
end