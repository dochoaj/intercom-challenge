# frozen_string_literal: true

require_relative '../../services/user_parser'

RSpec.describe Services::UserParser do
  describe 'parse_lines' do
    it 'should be defined' do
      expect(Services::UserParser.respond_to?(:parse_lines)).to be true
    end

    describe 'when input is a valid string array' do
      before(:each) do
        @input = ['{"latitude": "52.986375", "user_id": 12,
        "name": "Christina McArdle", "longitude": "-6.043701"}']
      end

      it 'should return an array' do
        expect(Services::UserParser.parse_lines(@input).class).to eq(Array)
      end

      it 'returned array should have the same length as input length' do
        expect(Services::UserParser.parse_lines(@input).length).to eq(
          @input.length
        )
      end

      it 'all returned array elements should be hashes' do
        expect(
          Services::UserParser.parse_lines(@input).all? { |el| el.is_a?(Hash) }
        ).to be true
      end

      it 'all returned array elements keys should be equals to expected keys length' do
        expect(
          Services::UserParser.parse_lines(@input).all? do |el|
            el.keys.length == Services::UserParser.expected_keys.length
          end
        ).to be true
      end
    end
  end

  describe 'parse_line' do
    it 'should be defined' do
      expect(Services::UserParser.respond_to?(:parse_line)).to be true
    end

    describe 'when input is nil' do
      it 'should raise an error when evaluating' do
        expect do
          Services::UserParser.parse_lines(nil)
        end.to raise_error Services::ParsingError
      end
    end

    describe 'when input is empty' do
      it 'should return an empty array' do
        returned = Services::UserParser.parse_lines []
        expect(returned.is_a?(Array)).to be true
        expect(returned.empty?).to be true
      end
    end

    describe 'when input is a valid expected line' do
      before(:each) do
        @line = '{"latitude": "52.986375", "user_id": 12,
        "name": "Christina McArdle", "longitude": "-6.043701"}'
      end

      it 'should return a hash' do
        expect(Services::UserParser.parse_line(@line).class).to eq(Hash)
      end

      it 'returned hashed should have the same key length of expected keys' do
        expect(Services::UserParser.parse_line(@line).keys.length).to eq(
          Services::UserParser.expected_keys.length
        )
      end
    end

    describe 'when input is nil' do
      it 'should raise an error when evaluating' do
        expect do
          Services::UserParser.parse_line(nil)
        end.to raise_error Services::ParsingError
      end
    end

    describe 'when input is only spaces' do
      it 'should raise an error when evaluating' do
        expect do
          Services::UserParser.parse_line('    ')
        end.to raise_error Services::ParsingError
      end
    end

    describe 'when input is empty' do
      it 'should raise an error when evaluating' do
        expect do
          Services::UserParser.parse_line('')
        end.to raise_error Services::ParsingError
      end
    end

    describe 'when input does not generates matchs' do
      it 'should raise an error when evaluating' do
        expect do
          Services::UserParser.parse_line('asd')
        end.to raise_error Services::ParsingError
      end
    end

    describe 'when input generates an incomplete match' do
      it 'should raise an error when evaluating' do
        expect do
          Services::UserParser.parse_line('{"latitude": "52.986375"}')
        end.to raise_error Services::ParsingError
      end
    end

    describe 'when input has a wrong syntax' do
      it 'should raise an error when evaluating' do
        expect do
          Services::UserParser.parse_line('{"latitude": "52.986375"')
        end.to raise_error Services::ParsingError
      end
    end
  end

  describe 'expected_keys' do
    it 'should be defined' do
      expect(Services::UserParser.respond_to?(:expected_keys)).to be true
    end

    it 'should return an array' do
      keys = Services::UserParser.expected_keys
      expect(keys.class).to eq(Array)
    end

    it 'returned array should contain only strings' do
      keys = Services::UserParser.expected_keys
      expect(keys.all? { |k| k.is_a? String }).to be true
    end

    it 'returned array should contain at least one element' do
      keys = Services::UserParser.expected_keys
      expect(keys.length).to be >= 1
    end
  end

  describe 'parse_valid' do
    it 'should be defined' do
      expect(Services::UserParser.respond_to?(:parse_valid?)).to be true
    end

    describe 'when testing hashes' do
      before(:each) do
        @expected_valid = Services::UserParser.expected_keys.each_with_object({}) do |k, h|
          h[k] = k
        end
        @expected_failure = @expected_valid.slice(Services::UserParser.expected_keys.first)
      end

      it 'should recognize a valid hash' do
        expect(Services::UserParser.parse_valid?(@expected_valid)).to be true
      end

      it 'should recognize an invalid hash when not having all keys' do
        expect(Services::UserParser.parse_valid?(@expected_failure)).to be false
      end

      it 'should recognize an invalid hash when hash is empty' do
        expect(Services::UserParser.parse_valid?({})).to be false
      end

      it 'should quick return false when hash is undefined' do
        expect(Services::UserParser.parse_valid?(nil)).to be false
      end
    end
  end

  describe 'error' do
    it 'should be defined' do
      expect(Services::UserParser.respond_to?(:error)).to be true
    end

    it 'should raise error_class with message' do
      message = 'Hello error'
      expect do
        Services::UserParser.error(message)
      end.to raise_error Services::UserParser.error_class, message
    end

    it 'should raise error_class with nil message' do
      message = nil
      expect do
        Services::UserParser.error(message)
      end.to raise_error Services::UserParser.error_class
    end
  end

  describe 'error_class' do
    it 'should be defined' do
      expect(Services::UserParser.respond_to?(:error_class)).to be true
    end

    it 'should respond with Services::ParsingError' do
      expect(Services::UserParser.error_class).to eq(Services::ParsingError)
    end
  end
end
