# frozen_string_literal: true

require_relative '../../services/data_output'

RSpec.describe Services::DataOutput do
  describe 'export' do
    before(:all) do
      @data = [
        { 'name' => 'Daniel', id: 1 }
      ]

      @exported_keys = ['name']
    end

    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:export)).to be true
    end

    it 'should call validate_data with correct parameter' do
      expect(Services::DataOutput).to receive(:validate_data).with(@data)
      Services::DataOutput.export(@data, @exported_keys)
    end

    it 'should call validate_exported_keys with correct parameter' do
      expect(Services::DataOutput).to receive(:validate_exported_keys).with(@exported_keys)
      Services::DataOutput.export(@data, @exported_keys)
    end

    it 'should call validate_format with correct parameter' do
      expect(Services::DataOutput).to receive(:validate_format).with('stdout')
      Services::DataOutput.export(@data, @exported_keys)
    end

    it 'should call export method with correct parameters' do
      expect(Services::DataOutput).to receive(
        Services::DataOutput.method_name_for 'stdout'
      ).with(@data, @exported_keys)
      Services::DataOutput.export(@data, @exported_keys)
    end
  end

  describe 'validate_data' do
    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:validate_data)).to be true
    end

    it 'should raise when data is not an Array' do
      expect do
        Services::DataOutput.validate_data(1)
      end.to raise_error(Services::DataOutput.error_class)

      expect do
        Services::DataOutput.validate_data([])
      end.to_not raise_error
    end

    it 'should raise when data is Array but elements are not Hash' do
      expect do
        Services::DataOutput.validate_data([1, { a: 'a' }, 2])
      end.to raise_error(Services::DataOutput.error_class)

      expect do
        Services::DataOutput.validate_data([{ a: 'a', b: 'b' }])
      end.to_not raise_error
    end
  end

  describe 'validate_format' do
    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:validate_format)).to be true
    end

    it 'should raise when format is not a String' do
      expect do
        Services::DataOutput.validate_format(1)
      end.to raise_error(Services::DataOutput.error_class)
    end

    it 'should raise when format is not supported' do
      expect do
        Services::DataOutput.validate_format('format not supported')
      end.to raise_error(Services::DataOutput.error_class)

      expect do
        Services::DataOutput.validate_format('stdout')
      end.to_not raise_error
    end
  end

  describe 'validate_exported_keys' do
    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:validate_exported_keys)).to be true
    end

    it 'should raise when exported keys is not an array' do
      expect do
        Services::DataOutput.validate_exported_keys([1, 2, 3])
      end.to raise_error(Services::DataOutput.error_class)
    end

    it 'should raise when not all exported keys elements are Strings' do
      expect do
        Services::DataOutput.validate_exported_keys([1, 2, 'String'])
      end.to raise_error(Services::DataOutput.error_class)

      expect do
        Services::DataOutput.validate_exported_keys(['String'])
      end.to_not raise_error
    end
  end

  describe 'method_name_for' do
    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:method_name_for)).to be true
    end

    it 'should raise when format is not a String' do
      expect do
        Services::DataOutput.method_name_for([1, 2])
      end.to raise_error(Services::DataOutput.error_class)

      expect do
        Services::DataOutput.method_name_for('stdout')
      end.to_not raise_error
    end

    it 'should respond a named convention for a valid format' do
      format = 'stdout'
      method_name = Services::DataOutput.method_name_for(format)
      expect(method_name.is_a?(Symbol)).to be true
      expect(method_name).to eq("export_#{format}".to_sym)
    end
  end

  describe 'extract_keys_from_data' do
    before(:all) do
      @data = [
        { 'name' => 'Daniel', id: 1 },
        { 'name' => 'Daniel2', id: 2 },
        { 'name' => 'Daniel3', id: 3 },
        { 'name' => 'Daniel4', id: 4 }
      ]
    end

    it 'should be defined' do
      expect(
        Services::DataOutput.respond_to?(:extract_keys_from_data)
      ).to be true
    end

    describe 'when exporting_keys is empty' do
      it 'should return data without changes' do
        exporting_keys = []
        results = Services::DataOutput.extract_keys_from_data(
          @data, exporting_keys
        )
        expect(results.length == @data.length).to be true
        expect((results & @data).length == @data.length).to be true
      end
    end

    describe 'when exporting_keys has elements' do
      it 'elements returned should only contains exporting_keys elements as keys' do
        exporting_keys = ['name']
        results = Services::DataOutput.extract_keys_from_data(
          @data, exporting_keys
        )
        expect(results.length == @data.length).to be true
        expect((results & @data).length == @data.length).to be false
        expect(
          results.all? { |r| r.keys.length == 1 && r.keys.first == 'name' }
        ).to be true
      end
    end
  end

  describe 'export_stdout' do
    before(:all) do
      @data = [
        { 'name' => 'Daniel', id: 1 },
        { 'name' => 'Daniel2', id: 2 },
        { 'name' => 'Daniel3', id: 3 },
        { 'name' => 'Daniel4', id: 4 }
      ]
    end

    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:export_stdout)).to be true
    end

    it 'should have a consistent format' do
      response = Services::DataOutput.export_stdout(@data, [])
      expect(
        response.split("\n").length == @data.length
      ).to be true

      expect(
        response.split("\n").first.split('-').length == @data.first.keys.length
      ).to be true
    end

    it 'should have been called standard output method' do
      expect do
        Services::DataOutput.export_stdout([@data.first], [])
      end.to output("Daniel - 1\n").to_stdout
    end

    describe 'when exported keys is not empty' do
      before(:each) do
        @exported_keys = %w[id name]
      end

      it 'should have consistent format' do
        response = Services::DataOutput.export_stdout(@data, @exported_keys)
        expect(
          response.split("\n").length == @data.length
        ).to be true

        expect(
          response.split("\n").first.split('-').length == @exported_keys.length
        ).to be true
      end

      it 'should respect the order of expected keys' do
        response = Services::DataOutput.export_stdout(@data, @exported_keys)
        raw = response.split("\n").first.split('-')
        first_data = @data[0]
        expect(raw[0].to_s.strip).to eq first_data[@exported_keys[0]].to_s
        expect(raw[1].to_s.strip).to eq first_data[@exported_keys[1]].to_s
      end
    end
  end

  describe 'error' do
    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:error)).to be true
    end

    it 'should raise error_class with message' do
      message = 'Hello error'
      expect do
        Services::DataOutput.error(message)
      end.to raise_error Services::DataOutput.error_class, message
    end

    it 'should raise error_class with nil message' do
      message = nil
      expect do
        Services::DataOutput.error(message)
      end.to raise_error Services::DataOutput.error_class
    end
  end

  describe 'error_class' do
    it 'should be defined' do
      expect(Services::DataOutput.respond_to?(:error_class)).to be true
    end

    it 'should respond with Services::DataOutputError' do
      expect(
        Services::DataOutput.error_class
      ).to eq(Services::DataOutputError)
    end
  end
end
