# frozen_string_literal: true

require_relative 'process/location_filter'
require 'optparse'

# Options parser
class InputParser
  def self.parse(args)
    options = {}
    opts = OptionParser.new do |opt|
      opt.on(
        '-u', '--source-url SOURCE URL', String,
        'URL from where read the users.'
      ) do |url|
        options[:source_url] = url
      end

      opt.on(
        '-p', '--path PATH', String,
        'File path from where read the users.
        This has priority over SOURCE URL.'
      ) do |path|
        options[:file_path] = path
      end

      opt.on(
        '-d', '--distance DISTANCE', Integer,
        'Distance (kms) used as tolerance when filtering users. Defaults to 100.'
        ) do |distance|
        options[:distance_limit] = distance
      end

      opt.on(
        '-x', '--latitude LATITUDE', Float,
        'Latitude for the point used as a reference point.
        Defaults to Intercom Dublin HQ.'
      ) do |lat|
        options[:center_lat] = lat
      end

      opt.on(
        '-y', '--longitude LONGITUDE', Float,
        'Longitude for the point used as a reference point.
        Defaults to Intercom Dublin HQ.'
      ) do |lon|
        options[:center_lon] = lon
      end
    end

    opts.parse(args)
    options
  end
end

begin
  options = InputParser.parse(ARGV)
  location_filter = Process::LocationFilter.new(options)
  location_filter.run
rescue StandardError => e
  puts "Error: #{e.message}. Run ruby main.rb --help for documentation."
end
