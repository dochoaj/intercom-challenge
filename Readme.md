# Daniel's Implementation of Intercom Challenge

## Challenge

We have some customer records in a text file (customers.txt) -- one customer per line, JSON lines formatted. We want to invite any customer within 100km of our Dublin office for some food and drinks on us. Write a program that will read the full list of customers and output the names and user ids of matching customers (within 100km), sorted by User ID (ascending). We recommend you use whatever language you feel strongest in. It doesn't have to be one we use!

## Solution

A CLI written using Ruby (2.5.0).

![Preview](http://g.recordit.co/3RzYNAgMtn.gif)

### Getting started

- Ruby 2.5.0, I recommend rbenv (https://github.com/rbenv/rbenv) to manage ruby versions.

- Bundler gem, run `gem install bundler` after switching ruby to 2.5.0. This gem allows to program dependencies to be installed.

- Install dependencies, run `bundle` inside of project directory.

- Run `ruby main.rb --help` to have a general view about the CLI.

- The test suite, run `bundle exec rspec` to run all test cases.

- For a quick output run `ruby main.rb -u https://s3.amazonaws.com/intercom-take-home-test/customers.txt`. This will output a list to terminal with the users that you need to invite to Dublin HQ, please also send me an invitation, I really want those drinks and food :)


### Parameters

```
Usage: main [options]
    -u, --source-url SOURCE URL      URL from where read the users.
    -p, --path PATH                  File path from where read the users.
        This has priority over SOURCE URL.
    -d, --distance DISTANCE          Distance (kms) used as tolerance when filtering users. Defaults to 100.
    -x, --latitude LATITUDE          Latitude for the point used as a reference point.
        Defaults to Intercom Dublin HQ.
    -y, --longitude LONGITUDE        Longitude for the point used as a reference point.
        Defaults to Intercom Dublin HQ.
```

### Design

The solution have been separated in three main concerns:

- (1) The interface to get user interaction: In this case the solution is a CLI tool.

- (2) The services to do some small, domain independent tasks.

- (3) The process, to take advantage of the application services and operate them to solve a problem.

#### Services

- Data Output: It outputs an array of hashes (key/value pairs) into a desired format. For the purpose of this challenge, only the stdout implementation is done. But the extensibility for other formats like csv, html, or even pdf is possible without breaking the internal API.

- Distance Calculator: Calculates the distance, in kilometers, of two lat-lon points represented as an array of two array of Floats (`[[x1, y1], [x2, y2]]`). The library used to calculate the distance is haversine (https://github.com/kristianmandrup/haversine), that uses the same algorithm proposed in the problem rubric but it is already well tested.

- Txt Reader: Reads a Txt file line by line and outputs it's content as an Array of Strings. This allows the service to be domain agnostic, the transformation of the Txt to some other thing, like a user, must be done somewhere else. This service can read files from local drive or from the web. When reading from the web a temporary local file is created to work huge files by chunks.

- User Parser: Transform a single JSON String or an Array of JSON Strings into User Hash (or Hashes). This service expects the strings to be well formed JSON data and also that the resulting Hash contains some fixed keys, like latitude or longitude. This is the most domain specific service and can be improved to be more agnostic, but works for the purpose of this challenge.

#### Process

**Location Filter**: This process reads a user list from the web or from local drive (Txt Reader + User Parser) and then filters the users depending of it's distance (Distance Calculator) to a certain point. After that, it outputs the filtered users to terminal (Data Output).

The implementation of the Process class is by composition, any instance of this class knows the existence of an outputter, reader, parser or distance calculator, but the definition of who are the objects thats provides the functionality are coupled just once.

This can be refactored to process to expect some abstract class for all the services, so we can be sure that internal API will not broke if we change a service. But, again, this works for the purposes of the challenge, I did write some tests that will broke if the internal API of one of the services changes.